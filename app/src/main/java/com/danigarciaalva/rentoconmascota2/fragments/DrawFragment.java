package com.danigarciaalva.rentoconmascota2.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danigarciaalva.rentoconmascota2.R;
import com.danigarciaalva.rentoconmascota2.TabsActivity;
import com.danigarciaalva.rentoconmascota2.interfaces.OnDrawToolbarClick;
import com.danigarciaalva.rentoconmascota2.interfaces.OnPhotoCapture;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by caprinet on 10/18/14.
 */
public class DrawFragment extends Fragment implements OnDrawToolbarClick, OnPhotoCapture{

    private Paint mPaint;
    private int color;
    private int strokeWidth;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        color = Color.BLUE;
        mPaint.setDither(true);
        mPaint.setColor(color);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        strokeWidth = 12;
        mPaint.setStrokeWidth(strokeWidth);

        TabsActivity activity = (TabsActivity)getActivity();
        activity.setDrawCallback(this);
        activity.setPhotoCallback(this);
    }

    private Lienzo lienzo;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        lienzo = new Lienzo(getActivity());
        return lienzo;
    }

    @Override
    public void onItemClick(int id) {
        switch (id){
        case R.id.act_main_tab_pencil:
            mPaint.setStrokeWidth(4f);
            break;
        case R.id.act_main_tab_pen:
            mPaint.setStrokeWidth(12f);
            break;
        case R.id.act_main_tab_camera:
            takePhoto();
            break;
        case R.id.act_main_tab_colors:
            break;
        }
    }

    private String name_file;
    private String path_file;

    private void takePhoto() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePicture.resolveActivity(getActivity().getPackageManager()) != null){
            File file= null;
            try{
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                name_file = "JPEG_"+timeStamp;
                File pictures = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                File image = File.createTempFile(name_file, ".jpg", pictures);
                path_file = image.getAbsolutePath();
                if(image != null){
                    takePicture.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
                    TabsActivity activity = (TabsActivity)getActivity();
                    activity.startActivityForResult(takePicture, 9999);
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }else{
            Toast.makeText(getActivity(), "No está la cámara disponible", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPhotoTaken(Intent data) {
        Toast.makeText(getActivity(), "Se tomo la foto", Toast.LENGTH_SHORT).show();
        Bitmap bitmap = BitmapFactory.decodeFile(path_file);
        if(bitmap != null) {
            Toast.makeText(getActivity(), "Imagen no nula", Toast.LENGTH_SHORT).show();
            lienzo.setBitmap(bitmap);
        }
    }

    public class Lienzo extends View{

        private int width;
        private int height;
        private Bitmap mBitmap;
        private Canvas mCanvas;
        private Path mPath;
        private Paint mBitmapPaint;
        private Context context;
        private Paint circlePaint;
        private Path circlePath;

        public Lienzo(Context context) {
            super(context);
            this.context = context;
            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);
            circlePaint = new Paint();
            circlePath = new Path();
            circlePaint.setAntiAlias(true);
            circlePaint.setColor(Color.YELLOW);
            circlePaint.setStyle(Paint.Style.STROKE);
            circlePaint.setStrokeJoin(Paint.Join.MITER);
            circlePaint.setStrokeWidth(4f);
        }


        public void setBitmap(Bitmap bitmap){
            this.mBitmap = bitmap;
            invalidate();
        }


        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            mBitmap = Bitmap.createBitmap(w,h, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up(x,y);
                    invalidate();
                    break;
            }
            return true;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawBitmap(mBitmap, 0, 0, null);
            //canvas.drawPath(circlePath, circlePaint);
        }

        private void touch_up(float x, float y) {
            mPath.lineTo(mX, mY);
            circlePath.reset();
            mCanvas.drawPath(mPath, mPaint);
            mPath.reset();
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if(dx >= 4 || dy >= 4){
                mPath.quadTo(mX, mY, (x+mX)/2, (y+mY)/2);
                mX = x;
                mY = y;
            }
        }

        float mX, mY;
        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x,y);
            mX = x;
            mY = y;
        }
    }
}

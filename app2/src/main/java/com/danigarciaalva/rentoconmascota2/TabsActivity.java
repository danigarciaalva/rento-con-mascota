package com.danigarciaalva.rentoconmascota2;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;

import com.danigarciaalva.rentoconmascota2.adapters.TabsAdapter;
import com.danigarciaalva.rentoconmascota2.interfaces.OnDrawToolbarClick;
import com.danigarciaalva.rentoconmascota2.interfaces.OnPhotoCapture;

/**
 * Created by caprinet on 10/18/14.
 */
public class TabsActivity extends FragmentActivity implements ActionBar.TabListener, ViewPager.OnPageChangeListener, View.OnClickListener {


    private ViewPager viewPager;
    private ActionBar actionBar;
    private TabsAdapter adapter;
    private String[] titles = {"Draw", "Photo", "Media"};
    private ImageButton pencil, pen, camera, colors;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tabs);
        viewPager = (ViewPager)findViewById(R.id.act_main_tabs_pager);
        pencil = (ImageButton)findViewById(R.id.act_main_tab_pencil);
        pen = (ImageButton)findViewById(R.id.act_main_tab_pen);
        camera = (ImageButton)findViewById(R.id.act_main_tab_camera);
        colors = (ImageButton)findViewById(R.id.act_main_tab_colors);
        pen.setOnClickListener(this);
        colors.setOnClickListener(this);
        camera.setOnClickListener(this);
        pencil.setOnClickListener(this);
        actionBar = getActionBar();
        adapter = new TabsAdapter(getSupportFragmentManager());
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        for(String s : titles){
            actionBar.addTab(actionBar.newTab().setText(s).setTabListener(this));
        }
        viewPager.setOnPageChangeListener(this);
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        actionBar.setSelectedNavigationItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private OnDrawToolbarClick callback;
    private OnPhotoCapture callbackPhoto;

    public void setPhotoCallback(OnPhotoCapture callback){
        this.callbackPhoto = callback;
    }

    public void setDrawCallback(OnDrawToolbarClick callback){
        this.callback = callback;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if(callbackPhoto != null)
                callbackPhoto.onPhotoTaken(data);
        }
    }

    @Override
    public void onClick(View view) {
        if(callback != null){
            callback.onItemClick(view.getId());
        }
    }
}

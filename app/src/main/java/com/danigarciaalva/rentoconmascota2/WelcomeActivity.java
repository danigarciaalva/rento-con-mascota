package com.danigarciaalva.rentoconmascota2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.danigarciaalva.rentoconmascota2.models.Anuncio;
import com.danigarciaalva.rentoconmascota2.models.AnuncioWrapper;
import com.danigarciaalva.rentoconmascota2.utils.Api;
import com.danigarciaalva.rentoconmascota2.utils.Statics;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;


public class WelcomeActivity extends Activity implements Api.OnApiCallDownload<String> {

    @InjectView(R.id.act_welcome_edt_mail)FormEditText editMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.inject(this);
        getActionBar().hide();
        editMail.setText("abc@dce.com");
    }

    @OnClick(R.id.act_welcome_btn_enter)
    public void onEnter(Button v){
        doPostCallback();
        if(editMail.testValidity()) {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("email", editMail.getText().toString().trim());
            new Api<Void, String>(this).postWithParams(params, "http://danielgarcia.dragonflylabs.com.mx/rcm/registro/", String.class, this);
        }
    }

    private void doPostCallback() {
        SharedPreferences.Editor editor = WelcomeActivity.this.getSharedPreferences(Statics.PREFS, Context.MODE_PRIVATE).edit();
        editor.putBoolean(Statics.PREFS_LOGGEDIN, true);
        editor.commit();
        Intent i = new Intent(WelcomeActivity.this, TabsActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onSuccess(String object) {
        if(object.equals("Ok")){
            doPostCallback();
        }else{
            Toast.makeText(WelcomeActivity.this, "Usuario incorrecto", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onError(String error) {

    }
}

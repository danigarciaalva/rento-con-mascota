package com.danigarciaalva.rentoconmascota2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.danigarciaalva.rentoconmascota2.utils.Statics;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;


public class WelcomeActivity extends Activity implements View.OnClickListener {

    Button btnEnter;
    EditText editMail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        getActionBar().hide();
        btnEnter = (Button)findViewById(R.id.act_welcome_btn_enter);
        btnEnter.setOnClickListener(this);
        editMail = (EditText)findViewById(R.id.act_welcome_edt_mail);
        editMail.setText("abc@dce.com");

    }

        @Override
        public void onClick(View view) {
            if(view.getId() == R.id.act_welcome_btn_enter){
                WelcomeTask task = new WelcomeTask();
                task.execute();
            }
        }

    class WelcomeTask extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... voids) {
            try{
                URI url = new URI("http://danielgarcia.dragonflylabs.com.mx/rcm/registro/");
                HttpResponse response = null;
                HttpPost post = new HttpPost(url);

                ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
                params.add(new BasicNameValuePair("email",editMail.getText().toString()));

                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, HTTP.UTF_8);
                post.setEntity(entity);
                HttpClient client = new DefaultHttpClient();
                response = client.execute(post);


                BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                String cad;
                StringBuilder sb = new StringBuilder();
                while((cad = br.readLine()) != null){
                    sb.append(cad);
                }
                String respuesta = sb.toString();
                System.out.println(respuesta);
                if(respuesta.equals("Ok"))
                    return true;
                return false;
            }catch(Exception e){
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean == true){
                SharedPreferences.Editor editor = WelcomeActivity.this.getSharedPreferences(Statics.PREFS, Context.MODE_PRIVATE).edit();
                editor.putBoolean(Statics.PREFS_LOGGEDIN, true);
                editor.commit();
                Intent i = new Intent(WelcomeActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }else{
                Toast.makeText(WelcomeActivity.this, "Usuario incorrecto", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

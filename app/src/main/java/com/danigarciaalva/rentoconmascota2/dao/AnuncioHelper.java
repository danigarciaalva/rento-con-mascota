package com.danigarciaalva.rentoconmascota2.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.danigarciaalva.rentoconmascota2.models.Anuncio;

/**
 * Created by caprinet on 11/8/14.
 */
public class AnuncioHelper extends SQLiteOpenHelper{

    // /data/data/<APP_NAME>/databases/
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "anuncios.db";


    public AnuncioHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        AnuncioTable.onCreate(database);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int i, int i2) {
        AnuncioTable.onCreate(database);
    }
}

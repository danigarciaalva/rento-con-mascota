package com.danigarciaalva.rentoconmascota2.interfaces;

import android.content.Intent;

/**
 * Created by caprinet on 10/18/14.
 */
public interface OnPhotoCapture {

    void onPhotoTaken(Intent data);
}

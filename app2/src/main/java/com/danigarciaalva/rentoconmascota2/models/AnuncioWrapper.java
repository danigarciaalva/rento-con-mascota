package com.danigarciaalva.rentoconmascota2.models;


import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AnuncioWrapper {

    private List<Anuncio> anuncios = new ArrayList<Anuncio>();
    private Meta meta;
    @Expose(deserialize =  false)
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<Anuncio> getAnuncios() {
        return anuncios;
    }

    public void setAnuncios(List<Anuncio> anuncios) {
        this.anuncios = anuncios;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
package com.danigarciaalva.rentoconmascota2.interfaces;

/**
 * Created by caprinet on 10/18/14.
 */
public interface OnDrawToolbarClick {

    void onItemClick(int id);
}

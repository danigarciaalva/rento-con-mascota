package com.danigarciaalva.rentoconmascota2.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.danigarciaalva.rentoconmascota2.fragments.DrawFragment;
import com.danigarciaalva.rentoconmascota2.fragments.MediaFragment;
import com.danigarciaalva.rentoconmascota2.fragments.PhotoFragment;

/**
 * Created by caprinet on 10/18/14.
 */
public class TabsAdapter extends FragmentPagerAdapter{

    public TabsAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new DrawFragment();
            case 1:
                return new PhotoFragment();
            case 2:
                return new MediaFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 1;
    }
}

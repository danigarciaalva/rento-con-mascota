package com.danigarciaalva.rentoconmascota2.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.danigarciaalva.rentoconmascota2.models.Anuncio;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by caprinet on 11/8/14.
 */
public class AnuncioDataSource {

    private SQLiteDatabase database;
    private DAOHelper dbHelper;
    private String[] allColumns = { AnuncioTable.COLUMN_ID,
            AnuncioTable.COLUMN_ID_SERVER,
            AnuncioTable.COLUMN_TITLE,
            AnuncioTable.COLUMN_DESCRIPTION,
            AnuncioTable.COLUMN_ADDRESS,
            AnuncioTable.COLUMN_LOCATION,
            AnuncioTable.COLUMN_PRICE,
            AnuncioTable.COLUMN_TYPE,
            AnuncioTable.COLUMN_ANIMALS_ACCEPTED,
            AnuncioTable.COLUMN_ACTIVE,
            AnuncioTable.COLUMN_CONTACT_MAIL,
            AnuncioTable.COLUMN_CONTACT_PHONE,};

    public AnuncioDataSource(Context context) {
        dbHelper = new DAOHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Anuncio createAnuncio(Anuncio anuncio) {
        ContentValues values = new ContentValues();
        values.put(AnuncioTable.COLUMN_ID_SERVER, anuncio.getId());
                values.put(AnuncioTable.COLUMN_TITLE, anuncio.getTitle());
                values.put(AnuncioTable.COLUMN_DESCRIPTION, anuncio.getDescription());
                values.put(AnuncioTable.COLUMN_ADDRESS,anuncio.getAddress());
                values.put(AnuncioTable.COLUMN_LOCATION, anuncio.getLocation());
                values.put(AnuncioTable.COLUMN_PRICE, anuncio.getPrice());
                values.put(AnuncioTable.COLUMN_TYPE, anuncio.getType());
                values.put(AnuncioTable.COLUMN_ANIMALS_ACCEPTED, anuncio.getAnimalsAccepted());
                values.put(AnuncioTable.COLUMN_ACTIVE, anuncio.getActive() ? 1 : 0);
                values.put(AnuncioTable.COLUMN_CONTACT_MAIL, anuncio.getContactMail());
                values.put(AnuncioTable.COLUMN_CONTACT_PHONE, anuncio.getContactPhone1());
        long insertId = database.insert(AnuncioTable.TABLE_ANUNCIO, null,values);
        Cursor cursor = database.query(AnuncioTable.TABLE_ANUNCIO,
                allColumns, AnuncioTable.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Anuncio newAnuncio = cursorToAnuncio(cursor);
        cursor.close();
        return newAnuncio;
    }

    private Anuncio cursorToAnuncio(Cursor cursor) {
        return null;
    }

    public void deleteAnuncio(Anuncio comment) {
        long id = comment.getId();
        System.out.println("Comment deleted with id: " + id);
        database.delete(AnuncioTable.TABLE_ANUNCIO, AnuncioTable.COLUMN_ID
                + " = " + id, null);
    }

    public List<Anuncio> getAllAnuncios() {
        List<Comment> comments = new ArrayList<Comment>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_COMMENTS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Comment comment = cursorToComment(cursor);
            comments.add(comment);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return comments;
    }

    private Comment cursorToComment(Cursor cursor) {
        Comment comment = new Comment();
        comment.setId(cursor.getLong(0));
        comment.setComment(cursor.getString(1));
        return comment;
    }
}

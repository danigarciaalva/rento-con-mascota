package com.danigarciaalva.rentoconmascota2.models;


import com.google.gson.annotations.Expose;

import java.util.HashMap;
import java.util.Map;

public class Meta {

    private Integer limit;
    private Object next;
    private Integer offset;
    private Object previous;
    @Expose(deserialize =  false)
    private Integer totalCount;
    @Expose(deserialize =  false)
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Object getNext() {
        return next;
    }

    public void setNext(Object next) {
        this.next = next;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Object getPrevious() {
        return previous;
    }

    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
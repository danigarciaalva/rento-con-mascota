package com.danigarciaalva.rentoconmascota2.models;

/**
 * Created by caprinet on 10/25/14.
 */
public class Imagen {

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String image;
}

package com.danigarciaalva.rentoconmascota2.fragments;



import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;


import com.danigarciaalva.rentoconmascota2.R;
import com.danigarciaalva.rentoconmascota2.adapters.AnunciosAdapter;
import com.danigarciaalva.rentoconmascota2.dao.DataSource;
import com.danigarciaalva.rentoconmascota2.models.Anuncio;
import com.danigarciaalva.rentoconmascota2.models.AnuncioWrapper;
import com.danigarciaalva.rentoconmascota2.utils.Api;
import com.danigarciaalva.rentoconmascota2.utils.Connection;
import com.danigarciaalva.rentoconmascota2.utils.Statics;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class AnunciosFragment extends Fragment implements Api.OnApiCallDownload<AnuncioWrapper> {

    ArrayList<Anuncio> model_anuncios = new ArrayList<Anuncio>();
    @InjectView(R.id.fr_anuncios_list)ListView lista;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.fragment_anuncios, container, false);
        ButterKnife.inject(this, v);

        boolean cache_enabled = getActivity().getSharedPreferences(Statics.PREFS, Context.MODE_PRIVATE)
                .getBoolean(Statics.PREFS_CACHE_ENABLED, false);
        if(Connection.isConnected(getActivity())){
            //AnunciosTask task = new AnunciosTask();
            //task.execute(cache_enabled);
            Api a = new Api<Void,AnuncioWrapper>(getActivity());
            a.get(AnuncioWrapper.class, "http://danielgarcia.dragonflylabs.com.mx/mascota/v1/anuncio/?format=json", this);
        }else{
            if(cache_enabled){
                DataSource dataSource = new DataSource(getActivity());
                try {
                    dataSource.open();
                    model_anuncios = (ArrayList<Anuncio>)dataSource.getAllAnuncios();
                    Statics.anuncios = model_anuncios;
                    lista.setAdapter(new AnunciosAdapter(getActivity(), R.layout.item_anuncio, model_anuncios));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(getActivity(), "No hay conexión a Internet", Toast.LENGTH_SHORT).show();
            }
        }

        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    public void onSuccess(AnuncioWrapper object) {
        model_anuncios = (ArrayList<Anuncio>)object.getAnuncios();
        Statics.anuncios = model_anuncios;
        lista.setAdapter(new AnunciosAdapter(getActivity(), R.layout.item_anuncio, model_anuncios));
    }

    @Override
    public void onError(String error) {

    }

    class AnunciosTask extends AsyncTask<Boolean, Void, Boolean>{

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setIndeterminate(false);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage("Descargando anuncios");
            dialog.show();
        }

        @Override
        protected Boolean doInBackground(Boolean... args) {
            try{
                URL url = new URL("http://danielgarcia.dragonflylabs.com.mx/mascota/v1/anuncio/?format=json");
                URLConnection connection = url.openConnection();

                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                String cad;
                StringBuilder sb = new StringBuilder();
                while((cad = br.readLine()) != null){
                    sb.append(cad);
                }
                String json = sb.toString();


                model_anuncios = (ArrayList<Anuncio>)new Gson().fromJson(json, AnuncioWrapper.class).getAnuncios();
                Statics.anuncios = model_anuncios;
                System.out.println(model_anuncios.get(0).getTitle());

                if(args[0] == true){
                    System.out.println("Cache habilitado");
                    DataSource dataSource = new DataSource(getActivity());
                    dataSource.open();
                    dataSource.deleteAll();
                    for(Anuncio a : model_anuncios){
                        dataSource.insertAnuncio(a);
                    }
                    dataSource.close();
                }else{
                    System.out.println("Cache deshabilitado");
                }
                return true;
            }catch(Exception e){
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Hubo un error en el servicio", Toast.LENGTH_SHORT).show();
                    }
                });
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dialog.dismiss();
            lista.setAdapter(new AnunciosAdapter(getActivity(), R.layout.item_anuncio, model_anuncios));

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.anuncios, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_map){

        }
        return super.onOptionsItemSelected(item);
    }
}

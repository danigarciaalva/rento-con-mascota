package com.danigarciaalva.rentoconmascota2.utils;

import com.danigarciaalva.rentoconmascota2.models.Anuncio;

import java.util.ArrayList;

/**
 * Created by caprinet on 10/25/14.
 */
public class Statics {

    public final static String PREFS = "prefsRCM";
    public final static String PREFS_LOGGEDIN = "prefsRCMLoggedIn";
    public final static String PREFS_MESSAGE_CACHE = "prefsRCMMessageCache";
    public static final String PREFS_CACHE_ENABLED = "prefsRCMCacheEnabled";

    public static ArrayList<Anuncio> anuncios;
}

package com.danigarciaalva.rentoconmascota2.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by caprinet on 10/4/14.
 */
public class TabsPagerAdapter extends FragmentPagerAdapter{

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                // Top Rated fragment activity
                return new Fragment();
            case 1:
                // Games fragment activity
                return new Fragment();
            case 2:
                // Movies fragment activity
                return new Fragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}

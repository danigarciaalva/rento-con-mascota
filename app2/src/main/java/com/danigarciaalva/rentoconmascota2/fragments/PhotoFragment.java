package com.danigarciaalva.rentoconmascota2.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danigarciaalva.rentoconmascota2.R;

/**
 * Created by caprinet on 10/18/14.
 */
public class PhotoFragment extends Fragment{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photo, container, false);
        return v;
    }
}

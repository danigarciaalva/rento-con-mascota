package com.danigarciaalva.rentoconmascota2.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Anuncio {

    private Boolean active;
    private String address;
    @SerializedName("animals_accepted")
    private Integer animalsAccepted;
    @SerializedName("contact_mail")
    private String contactMail;
    @Expose(deserialize =  false)
    private String contactPhone1;
    @Expose(deserialize =  false)
    private String contactPhone2;
    private String description;
    @SerializedName("id")
    private Integer idServer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Expose(deserialize =  false)
    private int id;
    private List<Imagen> imagenes = new ArrayList<Imagen>();
    private String location;
    private String price;
    @Expose(deserialize =  false)
    private String pubDate;
    @Expose(deserialize =  false)
    private String resourceUri;
    private String title;
    private Integer type;
    @Expose(deserialize =  false)
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getAnimalsAccepted() {
        return animalsAccepted;
    }

    public void setAnimalsAccepted(Integer animalsAccepted) {
        this.animalsAccepted = animalsAccepted;
    }

    public String getContactMail() {
        return contactMail;
    }

    public void setContactMail(String contactMail) {
        this.contactMail = contactMail;
    }

    public String getContactPhone1() {
        return contactPhone1;
    }

    public void setContactPhone1(String contactPhone1) {
        this.contactPhone1 = contactPhone1;
    }

    public String getContactPhone2() {
        return contactPhone2;
    }

    public void setContactPhone2(String contactPhone2) {
        this.contactPhone2 = contactPhone2;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIdServer() {
        return idServer;
    }

    public List<Imagen> getImagenes() {
        return imagenes;
    }

    public void setImagenes(List<Imagen> imagenes) {
        this.imagenes = imagenes;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getResourceUri() {
        return resourceUri;
    }

    public void setResourceUri(String resourceUri) {
        this.resourceUri = resourceUri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
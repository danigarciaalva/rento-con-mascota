package com.danigarciaalva.rentoconmascota2.fragments;



import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.danigarciaalva.rentoconmascota2.R;
import com.danigarciaalva.rentoconmascota2.adapters.AnunciosAdapter;
import com.danigarciaalva.rentoconmascota2.models.Anuncio;
import com.danigarciaalva.rentoconmascota2.models.AnuncioWrapper;
import com.danigarciaalva.rentoconmascota2.utils.Statics;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class AnunciosFragment extends Fragment {

    ArrayList<Anuncio> model_anuncios = new ArrayList<Anuncio>();
    ListView lista;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_anuncios, container, false);
        lista = (ListView)v.findViewById(R.id.fr_anuncios_list);

        boolean cache_enabled = getActivity().getSharedPreferences(Statics.PREFS, Context.MODE_PRIVATE)
                .getBoolean(Statics.PREFS_CACHE_ENABLED, false);
        AnunciosTask task = new AnunciosTask();
        task.execute(cache_enabled);
        return v;
    }


    class AnunciosTask extends AsyncTask<Boolean, Void, Boolean>{

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setIndeterminate(false);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage("Descargando anuncios");
            dialog.show();
        }

        @Override
        protected Boolean doInBackground(Boolean... args) {
            try{
                URL url = new URL("http://danielgarcia.dragonflylabs.com.mx/mascota/v1/anuncio/?format=json");
                URLConnection connection = url.openConnection();

                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                String cad;
                StringBuilder sb = new StringBuilder();
                while((cad = br.readLine()) != null){
                    sb.append(cad);
                }
                String json = sb.toString();


                model_anuncios = (ArrayList<Anuncio>)new Gson().fromJson(json, AnuncioWrapper.class).getAnuncios();
                /*JSONObject jObj = new JSONObject(json);
                JSONArray anuncios = jObj.getJSONArray("anuncios");



                int len = anuncios.length();
                for(int i = 0; i < len ; i++){
                    Anuncio anuncio = new Anuncio();
                    JSONObject model = anuncios.getJSONObject(i);
                    anuncio.setActive(model.getBoolean("active"));
                    anuncio.setTitle(model.getString("title"));
                    anuncio.setAddress(model.getString("address"));
                    anuncio.setLocation(model.getString("location"));
                    anuncio.setContactMail(model.getString("contact_mail"));
                    anuncio.setAnimalsAccepted(model.getInt("animals_accepted"));
                    anuncio.setPrice(model.getString("price"));
                    JSONArray imagenes = model.getJSONArray("imagenes");
                    ArrayList<Imagen> anuncio_imagenes = new ArrayList<Imagen>();
                    for(int j = 0; j < imagenes.length(); j++){
                        JSONObject imagen = imagenes.getJSONObject(j);
                        Imagen model_image = new Imagen();
                        model_image.setImage(imagen.getString("image"));
                        anuncio_imagenes.add(model_image);
                    }
                    anuncio.setImagenes(anuncio_imagenes);
                    model_anuncios.add(anuncio);
                }
                */
                System.out.println(model_anuncios.get(0).getTitle());

                if(args[0] == true){
                    System.out.println("Cache habilitado");
                }else{
                    System.out.println("Cache deshabilitado");
                }
                return true;
            }catch(Exception e){
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Hubo un error en el servicio", Toast.LENGTH_SHORT).show();
                    }
                });
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dialog.dismiss();
            lista.setAdapter(new AnunciosAdapter(getActivity(), R.layout.item_anuncio, model_anuncios));

        }
    }


}

package com.danigarciaalva.rentoconmascota2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.danigarciaalva.rentoconmascota2.utils.Statics;

import java.util.Timer;
import java.util.TimerTask;


public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getActionBar().hide();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                boolean loggedIn = SplashActivity.this
                        .getSharedPreferences(Statics.PREFS, Context.MODE_PRIVATE)
                        .getBoolean(Statics.PREFS_LOGGEDIN, false);
                if(loggedIn != true) {
                    Intent intent = new Intent(SplashActivity.this, WelcomeActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                }

                finish();
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, 2000);
    }
}

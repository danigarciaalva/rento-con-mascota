package com.danigarciaalva.rentoconmascota2.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.danigarciaalva.rentoconmascota2.models.Anuncio;
import com.danigarciaalva.rentoconmascota2.models.Imagen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by caprinet on 11/8/14.
 */
public class DataSource {

    private SQLiteDatabase database;
    private AnuncioHelper helper;
    private String[] allColumns = {
            AnuncioTable.COLUMN_ID,
            AnuncioTable.COLUMN_ID_SERVER,
            AnuncioTable.COLUMN_TITLE,
            AnuncioTable.COLUMN_PRICE,
            AnuncioTable.COLUMN_LOCATION,
            AnuncioTable.COLUMN_IMAGE
    };

    public DataSource(Context context){
        helper = new AnuncioHelper(context);
    }

    public void open() throws SQLException{
        database = helper.getWritableDatabase();
    }

    public void close(){
        helper.close();
    }

    public Anuncio insertAnuncio(Anuncio anuncio){
        ContentValues values = new ContentValues();
        values.put(AnuncioTable.COLUMN_ID_SERVER, anuncio.getIdServer());
        values.put(AnuncioTable.COLUMN_TITLE, anuncio.getTitle());
        values.put(AnuncioTable.COLUMN_PRICE, anuncio.getPrice());
        values.put(AnuncioTable.COLUMN_LOCATION, anuncio.getLocation());
        if(anuncio.getImagenes().size() > 0)
            values.put(AnuncioTable.COLUMN_IMAGE, anuncio.getImagenes().get(0).getImage());

        long insertid = database.insert(AnuncioTable.TABLE_ANUNCIO, null, values);
        Cursor cursor = database.query(AnuncioTable.TABLE_ANUNCIO, allColumns, AnuncioTable.COLUMN_ID + " = ? ", new String[]{ String.valueOf(insertid)}, null, null, null);
        cursor.moveToFirst();
        Anuncio aux;
        if((aux = cursorToAnuncio(cursor)) != null){
            anuncio.setId(aux.getId());
            return anuncio;
        }else{
            return null;
        }
    }

    public Anuncio cursorToAnuncio(Cursor cursor){
        Anuncio anuncio = new Anuncio();
        try {
            anuncio.setId((int) cursor.getLong(0));
            anuncio.setTitle(cursor.getString(2));
            anuncio.setPrice(cursor.getString(3));
            anuncio.setLocation(cursor.getString(4));
            ArrayList<Imagen> imagenes = new ArrayList<Imagen>();
            Imagen imagen = new Imagen();
            imagen.setImage(cursor.getString(5));
            imagenes.add(imagen);
            anuncio.setImagenes(imagenes);
        }catch (Exception e ){
            return null;
        }
        return anuncio;
    }

    public List<Anuncio> getAllAnuncios(){
        List<Anuncio> anuncios = new ArrayList<Anuncio>();
        Cursor cursor = database.query(AnuncioTable.TABLE_ANUNCIO, allColumns, null,null,null,null,null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            anuncios.add(cursorToAnuncio(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return anuncios;
    }

    public void deleteAll(){
        database.delete(AnuncioTable.TABLE_ANUNCIO, null, null);
    }
}

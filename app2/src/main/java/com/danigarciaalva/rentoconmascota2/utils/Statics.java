package com.danigarciaalva.rentoconmascota2.utils;

/**
 * Created by caprinet on 10/25/14.
 */
public class Statics {

    public final static String PREFS = "prefsRCM";
    public final static String PREFS_LOGGEDIN = "prefsRCMLoggedIn";
    public final static String PREFS_MESSAGE_CACHE = "prefsRCMMessageCache";
    public static final String PREFS_CACHE_ENABLED = "prefsRCMCacheEnabled";
}

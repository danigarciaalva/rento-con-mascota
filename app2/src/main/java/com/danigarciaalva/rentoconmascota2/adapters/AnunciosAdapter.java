package com.danigarciaalva.rentoconmascota2.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.danigarciaalva.rentoconmascota2.R;
import com.danigarciaalva.rentoconmascota2.models.Anuncio;
import com.loopj.android.image.SmartImageView;

import java.util.ArrayList;

/**
 * Created by caprinet on 10/25/14.
 */
public class AnunciosAdapter extends ArrayAdapter<Anuncio>{

    private Context context;
    private ArrayList<Anuncio> items;

    public AnunciosAdapter(Context context, int resource, ArrayList<Anuncio> objects) {
        super(context, resource, objects);
        this.context = context;
        this.items = objects;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_anuncio, null);
            holder = new ViewHolder();
            holder.title = (TextView)convertView.findViewById(R.id.item_anuncio_title);
            holder.image = (SmartImageView)convertView.findViewById(R.id.item_anuncio_image);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        Anuncio model = getItem(position);
        holder.title.setText(model.getTitle());
        holder.image.setImageUrl("http://danielgarcia.dragonflylabs.com.mx"+model.getImagenes().get(0).getImage());
        return convertView;
    }


    class ViewHolder{

        public TextView title;
        public SmartImageView image;

    }
}

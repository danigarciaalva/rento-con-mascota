package com.danigarciaalva.rentoconmascota2.dao;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by caprinet on 11/8/14.
 */
public class AnuncioTable {

    public static final String TABLE_ANUNCIO = "anuncio";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_ID_SERVER = "_idServer";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_LOCATION = "location";
    public static final String COLUMN_IMAGE = "image";

    private static final String DATABASE_CREATE = "create table "
            +TABLE_ANUNCIO + "("
            +COLUMN_ID + " integer primary key autoincrement, " +
            COLUMN_ID_SERVER +" integer," +
            COLUMN_TITLE + " text," +
            COLUMN_PRICE + " text," +
            COLUMN_LOCATION + " text," +
            COLUMN_IMAGE + " text);";

    public static void onCreate(SQLiteDatabase database){
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
        database.execSQL("DROP TABLE IF EXISTS "+TABLE_ANUNCIO);
        onCreate(database);
    }
}

package com.danigarciaalva.rentoconmascota2.models;

/**
 * Created by caprinet on 10/4/14.
 */
public class NavDrawerItem {

    public NavDrawerItem(String title){
        this.title = title;
    }

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

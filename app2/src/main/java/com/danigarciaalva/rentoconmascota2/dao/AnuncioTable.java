package com.danigarciaalva.rentoconmascota2.dao;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by caprinet on 11/8/14.
 */
public class AnuncioTable {

    public static String TABLE_ANUNCIO = "anuncio";
    public static String COLUMN_ID = "_id";
    public static String COLUMN_ID_SERVER = "_idServer";
    public static String COLUMN_TITLE = "title";
    public static String COLUMN_DESCRIPTION = "description";
    public static String COLUMN_ADDRESS = "address";
    public static String COLUMN_LOCATION = "location";
    public static String COLUMN_PRICE = "price";
    public static String COLUMN_TYPE = "type";
    public static String COLUMN_ANIMALS_ACCEPTED = "animals_accepted";
    public static String COLUMN_ACTIVE = "active";
    public static String COLUMN_CONTACT_MAIL = "contact_mail";
    public static String COLUMN_CONTACT_PHONE = "contact_phone";

    private static final String DATABASE_CREATE = "create table "
            + TABLE_ANUNCIO
            +"("
            +COLUMN_ID + " integer primary key autoincrement, "
            +COLUMN_ID_SERVER + " integer, "
            +COLUMN_TITLE + " text not null, "
            +COLUMN_DESCRIPTION + " text, "
            +COLUMN_ADDRESS + " text, "
            +COLUMN_LOCATION + " text, "
            +COLUMN_PRICE + " text, "
            +COLUMN_TYPE + " integer, "
            +COLUMN_ANIMALS_ACCEPTED + " integer, "
            +COLUMN_ACTIVE + " integer, "
            +COLUMN_CONTACT_MAIL + " text, "
            +COLUMN_CONTACT_PHONE + " text);";

    public static void onCreate(SQLiteDatabase database){
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
        database.execSQL("drop table if exists "+TABLE_ANUNCIO);
        onCreate(database);
    }


}
